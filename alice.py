import argparse
from auxiliary import Auxiliary


def create_argument_parser():
    arg_parser = argparse.ArgumentParser(description='Sender of an oblivious transfer protocol')
    arg_parser.add_argument('message1', type=str, help='First message to send with probability 1/2')
    arg_parser.add_argument('message2', type=str, help='First message to send with probability 1/2')
    arg_parser.add_argument('-p', '--port', type=int, help='Port to use', default=65435)
    # Server cert: openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
    arg_parser.add_argument('-sc', '--server_cert', type=argparse.FileType('r'), default='certs/server.crt',
                            help='Server signed certificate in crt format')
    arg_parser.add_argument('-sk', '--server_key', type=argparse.FileType('r'), default='certs/server.key',
                            help='Server private key in key format')
    arg_parser.add_argument('-c', '--client_cert', type=argparse.FileType('r'), default='certs/client.crt',
                            help='Server signed certificate in crt format')
    return arg_parser


if __name__ == '__main__':
    parser = create_argument_parser()

    args = parser.parse_args()

    alice_port = args.port
    m1 = args.message1.encode('utf8')
    m2 = args.message2.encode('utf8')
    server_cert = args.server_cert.name
    server_key = args.server_key.name
    client_certs = args.client_cert.name

    aux = Auxiliary()

    aux.setup_ssl_connection_alice(alice_port, server_cert, server_key, client_certs)

    pk1, sk1, pk2, sk2 = aux.generate_RSA()

    aux.send_bytes(pk1.export_key())
    aux.send_bytes(pk2.export_key())

    key_len = aux.receive_integer()

    kk = aux.receive_bytes()

    k1 = aux.decrypt_RSA(sk1, kk, key_len)
    k2 = aux.decrypt_RSA(sk2, kk, key_len)

    c1, iv1 = aux.encrypt_AES(k1, m1)
    c2, iv2 = aux.encrypt_AES(k2, m2)

    aux.send_bytes(c1)
    aux.send_bytes(iv1)
    aux.send_bytes(c2)
    aux.send_bytes(iv2)

    print('Protocol finished. Closing connection with Bob')

    aux.close()
