import socket
import ssl
from Crypto.Random import get_random_bytes
from Crypto.PublicKey import RSA
from Crypto.Cipher import PKCS1_v1_5, AES


class Auxiliary:
    def __init__(self):
        self.socket = None

    def get_own_ip(self):
        hostname = socket.gethostname()
        ip_addr = socket.gethostbyname(hostname)
        return ip_addr

    def setup_ssl_connection_alice(self, receiving_port, server_cert, server_key, client_certs):
        try:
            context = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
            context.verify_mode = ssl.CERT_REQUIRED
            context.load_cert_chain(certfile=server_cert, keyfile=server_key)
            context.load_verify_locations(cafile=client_certs)

            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            ip_addr = self.get_own_ip()
            sock.bind((ip_addr, receiving_port))
            print(f'Your IP is {ip_addr} and your port is {receiving_port}')
            print('Waiting for Bob')
            sock.listen(5)
            conn, addr = sock.accept()
            print('Connection from Bob successful')
            ssl_conn = context.wrap_socket(conn, server_side=True, do_handshake_on_connect=True)
            self.socket = ssl_conn
        except ssl.SSLError as e:
            print(f'Error while creating the secure TLS connection: {e}')
            exit(3)
        except ssl.CertificateError as e:
            print(f'Error with the certificate provided: {e}')
            exit(4)
        print(f'TLS secure connection established with bob')

    def setup_ssl_connection_bob(self, address, receiving_port, server_hostname, server_cert, client_cert, client_key):
        try:
            context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=server_cert)
            context.load_cert_chain(certfile=client_cert, keyfile=client_key)

            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket = context.wrap_socket(self.socket, server_side=False, server_hostname=server_hostname,
                                              do_handshake_on_connect=True)
            print(f'Trying to connect to Alice at IP {address} port {receiving_port} using TLS')
            self.socket.connect((address, receiving_port))
        except TimeoutError:
            print('Alice could not be reached')
            exit(1)
        except ConnectionRefusedError:
            print('Alice refused the connection')
            exit(2)
        except ssl.SSLError as e:
            print(f'Error while creating the secure TLS connection: {e}')
            exit(3)
        except ssl.CertificateError as e:
            print(f'Error with the certificate provided: {e}')
            exit(4)
        print('Connection to Alice successful')

    def send_bytes(self, bytes_message):
        self.socket.send(bytes_message)

    def receive_bytes(self):
        received_bytes = b'0'
        try:
            received_bytes = self.socket.recv(1024)
        except ConnectionResetError as e:
            print(f'Error while receiving the message: {e}')
            exit(1)
        return received_bytes

    def close(self):
        self.socket.close()

    def send_integer(self, num):
        bytes_int = num.to_bytes(1024, byteorder='big')
        self.socket.send(bytes_int)

    def receive_integer(self):
        received_int = -1
        try:
            received_int = int.from_bytes(self.socket.recv(1024), byteorder='big')
        except ConnectionResetError as e:
            print(f'Error while receiving the message: {e}')
            exit(1)
        return received_int

    @staticmethod
    def generate_RSA():
        k1 = RSA.generate(1024)
        k2 = RSA.generate(1024)
        pk1 = k1.publickey()
        pk2 = k2.publickey()
        return pk1, k1, pk2, k2

    @staticmethod
    def decrypt_RSA(key, ciphertext, k_len):
        cipher = PKCS1_v1_5.new(key)
        try:
            return cipher.decrypt(ciphertext, get_random_bytes(k_len))
        except ValueError:
            return get_random_bytes(k_len)


    @staticmethod
    def encrypt_AES(key, plaintext):
        c = AES.new(key, AES.MODE_CFB)
        return c.encrypt(plaintext), c.iv

    @staticmethod
    def encrypt_RSA(key, plaintext):
        cipher = PKCS1_v1_5.new(key)
        return cipher.encrypt(plaintext)

    @staticmethod
    def decrypt_AES(key, ciphertext, iv):
        c = AES.new(key, AES.MODE_CFB, iv=iv)
        return c.decrypt(ciphertext)
