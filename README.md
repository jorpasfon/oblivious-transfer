# Oblivious transfer

In this repository there is the implementation of a 1-2 oblivious transfer protocol (Schneier, 1996)

## Installation

The implementation requires **python** and the requirements that can be found on the **requirements.txt** file.
They can be installed with the following command:

```sh
$ pip install -r requirements.txt
```

## Usage

There are two python files, **alice.py** and **bob.py**. The participant of the protocol that wants to send a message with 1/2 probability has to execute **alice.py**. The receiver of the protocol has to execute **bob.py**.

### Certificates

To establish a secure TLS communication between Alice and Bob, certificates are needed. Alice is considered a server and Bob a client when establishing the connections. 
To generate the certificates, openssl can be used for instance. Following, the openssl commands that generate RSA 2048 certificates are shown.

```sh
# Alice certificate
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout server.key -out server.crt
# Bob certificate
$ openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout client.key -out client.crt
```

Certificates generated with the previous commands are provided in the *certs* folder. The server hostname specified is oblivious.transfer, which if changed has to be specified as a parameter of Bob.
 A secure method to exchange the certificates between Alice and Bob should be used, and it is not provided in this repository.

### Alice

To execute the Alice program, 2 parameters must be provided, which correspond to the messages that have to be send to Bob with probability 1/2.
Assuming the provided certificates and the default port to create the TLS connection are used, the following command executes the Alice program.

```sh
$ python alice.py message1 message2
```

After executing this command, the IP address and the port Alice is listening for Bob are shown. They have to be provided to Bob so a connection between them can be established.

To obtain more details about the optional parameters such as the certificates or the port, the following help command can be used.

```sh
$ python alice.py --help
```

### Bob

To execute the Bob program, 1 parameter must be provided, which is the IP address of Alice.
Assuming the provided certificates and the default options are used on both Alice and Bob, and that the IP address of Alice is 127.0.1.1, the following command executes the Bob program.

```sh
$ python bob.py 127.0.1.1
```

After executing this command, one of the two message of alice will be shown with 1/2 probability.

To obtain more detauls about the optional parameters such as the certificates, port, length of the AES key, etc, the following help command can be used.

```sh
$ python bob.py --help
```
