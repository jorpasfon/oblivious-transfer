import argparse
from secrets import choice
from Crypto.Random import get_random_bytes
from Crypto.PublicKey import RSA
from auxiliary import Auxiliary


def create_argument_parser():
    arg_parser = argparse.ArgumentParser(description='Receiver of an oblivious transfer protocol')
    arg_parser.add_argument('address', type=str, help='IP address of Alice')
    arg_parser.add_argument('-l', '--length', type=int, help='Length in bytes for the randomly generated AES key'
                                                             ' [16, 24, 32]', default=32)
    arg_parser.add_argument('-p', '--port', type=int, help='Port of Alice', default=65435)
    # Client cert: openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -keyout client.key -out client.crt
    arg_parser.add_argument('-ho', '--server_hostname', type=str, default='oblivious.transfer',
                            help='Server certificate common name')
    arg_parser.add_argument('-sc', '--server_cert', type=argparse.FileType('r'), default='certs/server.crt',
                            help='Server signed certificate in crt format')
    arg_parser.add_argument('-cc', '--client_certificate', type=argparse.FileType('r'), default='certs/client.crt',
                            help='Client signed certificate in crt format')
    arg_parser.add_argument('-ck', '--client_key', type=argparse.FileType('r'), default='certs/client.key',
                            help='Client private key in key format')
    return arg_parser


def get_message(pk, pk1, pk2, c1, iv1, c2, iv2, key):
    message = ''
    if pk == pk1:
        print('Selected key 1')
        message = aux.decrypt_AES(key, c1, iv1).decode("utf8")
    elif pk == pk2:
        print('Selected key 2')
        message = aux.decrypt_AES(key, c2, iv2).decode("utf8")
    else:
        print('Incorrect Key!')
    return message


if __name__ == '__main__':
    parser = create_argument_parser()

    args = parser.parse_args()

    alice_address = args.address
    alice_port = args.port
    key_len = args.length
    server_hostname = args.server_hostname
    server_cert = args.server_cert.name
    client_cert = args.client_certificate.name
    client_key = args.client_key.name

    aux = Auxiliary()

    aux.setup_ssl_connection_bob(alice_address, alice_port, server_hostname, server_cert, client_cert, client_key)

    pk1 = aux.receive_bytes()
    pk2 = aux.receive_bytes()

    pk1 = RSA.import_key(pk1)
    pk2 = RSA.import_key(pk2)

    aux.send_integer(key_len)

    k = get_random_bytes(key_len)
    pk = choice([pk1, pk2])

    kk = aux.encrypt_RSA(pk, k)
    aux.send_bytes(kk)

    c1 = aux.receive_bytes()
    iv1 = aux.receive_bytes()
    c2 = aux.receive_bytes()
    iv2 = aux.receive_bytes()

    m = get_message(pk, pk1, pk2, c1, iv1, c2, iv2, k)

    print(f'Received message: {m}')

    aux.close()
